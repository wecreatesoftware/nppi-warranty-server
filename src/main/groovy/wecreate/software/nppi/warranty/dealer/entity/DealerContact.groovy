package wecreate.software.nppi.warranty.dealer.entity

import org.hibernate.envers.Audited
import wecreate.software.nppi.warranty.address.entity.Address
import wecreate.software.nppi.warranty.application.entity.DatabaseEntity

import javax.persistence.CascadeType
import javax.persistence.Entity
import javax.persistence.OneToOne

@Entity
@Audited
class DealerContact extends DatabaseEntity {

    String companyName

    String contactName

    @OneToOne(cascade = CascadeType.ALL)
    Address address

    String phoneNumber

    String technicianPerformingRepair
}
