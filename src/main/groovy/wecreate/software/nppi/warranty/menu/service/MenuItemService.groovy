package wecreate.software.nppi.warranty.menu.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import wecreate.software.nppi.warranty.menu.entity.MenuItem
import wecreate.software.nppi.warranty.menu.repository.MenuItemRepository
import wecreate.software.nppi.warranty.user.entity.User
import wecreate.software.nppi.warranty.user.service.UserService

@Service
class MenuItemService {

    @Autowired
    MenuItemRepository menuItemRepository

    @Autowired
    UserService userService

    List<MenuItem> findMenuItemsForLoggedInUser() {//TODO add caching
        User user = userService.findLoggedInUser()
        return menuItemRepository.findByRolesContainsAndDisabledOrderByDisplayOrder(user.roles, false)
    }
}
