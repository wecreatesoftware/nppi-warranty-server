package wecreate.software.nppi.warranty.menu.entity

import com.fasterxml.jackson.annotation.JsonIgnore
import org.hibernate.envers.Audited
import wecreate.software.nppi.warranty.application.entity.DatabaseEntity
import wecreate.software.nppi.warranty.role.entity.Role

import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.JoinColumn
import javax.persistence.JoinTable
import javax.persistence.ManyToMany
import javax.persistence.OneToMany

@Entity
@Audited
class MenuItem extends DatabaseEntity {

    Integer displayOrder

    String name

    String icon

    String href

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
        name = "menu_item_role",
        joinColumns = @JoinColumn(name = "menu_item_id"),
        inverseJoinColumns = @JoinColumn(name = "role_id")
    )
    @JsonIgnore
    List<Role> roles
}
