package wecreate.software.nppi.warranty.menu.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import wecreate.software.nppi.warranty.menu.entity.MenuItem
import wecreate.software.nppi.warranty.role.entity.Role

@Repository
interface MenuItemRepository extends JpaRepository<MenuItem, String> {

    List<MenuItem> findByRolesContainsAndDisabledOrderByDisplayOrder(List<Role> roles, Boolean disabled)
}
