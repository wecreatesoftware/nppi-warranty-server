package wecreate.software.nppi.warranty.menu.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import wecreate.software.nppi.warranty.menu.domain.Menu
import wecreate.software.nppi.warranty.menu.service.MenuItemService

@RestController
@RequestMapping("menu")
class MenuController {

    @Autowired
    MenuItemService menuItemService

    @GetMapping
    Menu menu(){
        return new Menu(menuItems: menuItemService.findMenuItemsForLoggedInUser())
    }
}
