package wecreate.software.nppi.warranty.role.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import wecreate.software.nppi.warranty.role.entity.Role
import wecreate.software.nppi.warranty.role.service.RoleService

@RestController
@RequestMapping("roles")
class RoleController {

    @Autowired
    RoleService roleService

    @GetMapping
    List<Role> roles() {
        roleService.findAll()
    }
}
