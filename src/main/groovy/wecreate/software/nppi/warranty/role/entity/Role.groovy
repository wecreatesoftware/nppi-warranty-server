package wecreate.software.nppi.warranty.role.entity

import org.hibernate.envers.Audited
import wecreate.software.nppi.warranty.application.audit.BetterAuditorAware
import wecreate.software.nppi.warranty.application.entity.DatabaseEntity

import javax.persistence.Entity
import javax.persistence.EntityListeners

@Entity
@Audited
class Role extends DatabaseEntity {

    String name

    String description
}
