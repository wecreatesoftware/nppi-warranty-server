package wecreate.software.nppi.warranty.role.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import wecreate.software.nppi.warranty.role.entity.Role
import wecreate.software.nppi.warranty.role.repository.RoleRepository

@Service
class RoleService {

    @Autowired
    RoleRepository roleRepository

    List<Role> findAll() {
        return roleRepository.findAll()
    }
}
