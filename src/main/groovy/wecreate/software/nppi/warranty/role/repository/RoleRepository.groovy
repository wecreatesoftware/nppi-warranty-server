package wecreate.software.nppi.warranty.role.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import wecreate.software.nppi.warranty.role.entity.Role

@Repository
interface RoleRepository extends JpaRepository<Role, String> {}
