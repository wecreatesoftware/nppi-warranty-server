package wecreate.software.nppi.warranty.user.repository

import org.springframework.data.jpa.repository.JpaRepository
import wecreate.software.nppi.warranty.user.entity.User

interface UserRepository extends JpaRepository<User, String> {

    User findByUsername(String username)
}
