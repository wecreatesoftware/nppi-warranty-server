package wecreate.software.nppi.warranty.user.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service
import wecreate.software.nppi.warranty.role.entity.Role
import wecreate.software.nppi.warranty.user.entity.User
import wecreate.software.nppi.warranty.user.repository.UserRepository

@Service
class UserService {

    @Autowired
    UserRepository userRepository

    List<User> findAll() {
        return userRepository.findAll()
    }

    User findByUsername(String username) {
        return userRepository.findByUsername(username)
    }

    User findLoggedInUser() {
        return userRepository.findByUsername(loggedInUsername())
    }

    static String loggedInUsername() {
        return SecurityContextHolder.context?.authentication?.principal ?: "system"
    }

    static List<Role> loggedInUserRoles() {
        return SecurityContextHolder.context?.authentication?.authorities?.role
    }

    Boolean isAdmin() {
        return loggedInUserRoles()?.contains("admin")
    }
}
