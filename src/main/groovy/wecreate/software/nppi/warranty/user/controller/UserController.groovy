package wecreate.software.nppi.warranty.user.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import wecreate.software.nppi.warranty.user.entity.User
import wecreate.software.nppi.warranty.user.service.UserService

@RestController
@RequestMapping("users")
class UserController {

    @Autowired
    UserService userService

    @GetMapping
    List<User> users() {
        userService.findAll()
    }
}
