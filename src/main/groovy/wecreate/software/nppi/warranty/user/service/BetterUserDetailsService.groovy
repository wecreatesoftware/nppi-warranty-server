package wecreate.software.nppi.warranty.user.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.authority.AuthorityUtils
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service

@Service
class BetterUserDetailsService implements UserDetailsService {

    @Autowired
    UserService userService

    @Override
    UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        wecreate.software.nppi.warranty.user.entity.User user = userService.findByUsername(username)
        if (!user) {
            throw new UsernameNotFoundException("User with username=${username} was not found.")
        }
        if (user.disabled) {
            throw new UsernameNotFoundException("User with username=${username} is disabled.")
        }
        List<String> authorities = user.roles.collect { it.name }
        new User(user.username, user.password, !user.disabled, true, true, true, AuthorityUtils.createAuthorityList(authorities as String[]))
    }
}
