package wecreate.software.nppi.warranty.user.entity

import com.fasterxml.jackson.annotation.JsonIgnore
import org.springframework.data.domain.Auditable
import wecreate.software.nppi.warranty.application.entity.DatabaseEntity
import wecreate.software.nppi.warranty.role.entity.Role

import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.JoinColumn
import javax.persistence.JoinTable
import javax.persistence.ManyToMany
import javax.persistence.OneToMany
import org.hibernate.envers.Audited

@Entity
@Audited
class User extends DatabaseEntity {

    String username

    String firstname

    String lastname

    @JsonIgnore
    String password

    String email

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "user_role",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id")
    )
    List<Role> roles
}
