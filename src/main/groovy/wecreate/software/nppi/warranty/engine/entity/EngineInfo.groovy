package wecreate.software.nppi.warranty.engine.entity

import org.hibernate.envers.Audited
import wecreate.software.nppi.warranty.application.entity.DatabaseEntity

import javax.persistence.Entity
import javax.persistence.Temporal
import javax.persistence.TemporalType

@Entity
@Audited
class EngineInfo extends DatabaseEntity {

    String engineSerialNumber

    String machineSerialNumber

    String engineHours

    @Temporal(TemporalType.TIMESTAMP)
    Date dateOfFailure

    @Temporal(TemporalType.TIMESTAMP)
    Date dateOfRepair
}
