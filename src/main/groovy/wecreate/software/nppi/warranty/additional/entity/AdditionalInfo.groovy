package wecreate.software.nppi.warranty.additional.entity

import org.hibernate.envers.Audited
import wecreate.software.nppi.warranty.application.entity.DatabaseEntity

import javax.persistence.Entity

@Entity
@Audited
class AdditionalInfo extends DatabaseEntity {

    String customerConcerns

    String additionalDetails

    String repairPerformed

    String failureCause
}
