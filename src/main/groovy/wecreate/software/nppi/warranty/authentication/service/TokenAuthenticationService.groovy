package wecreate.software.nppi.warranty.authentication.service

import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.authority.AuthorityUtils
import javax.servlet.http.Cookie
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class TokenAuthenticationService {

    static final Integer EXPIRATIONTIME = 3600
    static final String SECRET = "ThisIsASecret"

    static void addAuthentication(HttpServletRequest request, HttpServletResponse response, String username) {
        String JWT = Jwts.builder()
            .setSubject(username)
            .setExpiration(new Date(System.currentTimeMillis() + (EXPIRATIONTIME * 60 * 60)))
            .signWith(SignatureAlgorithm.HS512, SECRET)
            .compact()
        Cookie cookie = new Cookie("session", JWT)
        cookie.maxAge = EXPIRATIONTIME
        response.addCookie(cookie)
    }

    static Authentication getAuthentication(HttpServletRequest request) {
        Cookie token = request.cookies.find { it.name == "session" }
        if (token != null) {
            //TODO if expired, renew ...
            String user = Jwts.parser()
                .setSigningKey(SECRET)
                .parseClaimsJws(token.value)
                .getBody()
                .getSubject()
//TODO: get user from database


            return user != null ?
                new UsernamePasswordAuthenticationToken(user, null, AuthorityUtils.createAuthorityList([user] as String[])) :
                null
        }
        return null
    }
}