package wecreate.software.nppi.warranty.authentication.handlers

import org.springframework.security.core.Authentication
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler

import javax.servlet.ServletException
import javax.servlet.http.Cookie
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class BetterLogoutSuccessHandler implements LogoutSuccessHandler {

    @Override
    void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        Cookie cookie = new Cookie("session", null)
        cookie.path = "/"
        cookie.httpOnly = true
        cookie.maxAge = 0
        response.addCookie(cookie)
        response.status = HttpServletResponse.SC_OK
    }

}