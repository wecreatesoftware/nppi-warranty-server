package wecreate.software.nppi.warranty.application.views

class JsonViews {
    interface AdminView {}
    interface NonAdminView {}

    interface AllView extends NonAdminView, AdminView {}
}
