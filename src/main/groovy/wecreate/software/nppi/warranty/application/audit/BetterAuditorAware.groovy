package wecreate.software.nppi.warranty.application.audit

import org.springframework.context.annotation.Configuration
import org.springframework.data.domain.AuditorAware
import wecreate.software.nppi.warranty.user.service.UserService

@Configuration
class BetterAuditorAware implements AuditorAware<String> {

    @Override
    String getCurrentAuditor() {
        return new Random().nextInt() % 2 == 0 ? "admin" : "user"

        //return UserService.loggedInUsername()
    }
}
