package wecreate.software.nppi.warranty.application.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.cors.CorsConfiguration
import org.springframework.web.cors.CorsConfigurationSource
import org.springframework.web.cors.UrlBasedCorsConfigurationSource

@Configuration
class CorsConfig {
    @Bean
    CorsConfigurationSource corsConfigurationSource() {
        // this is allowing cors for all non permit all urls, all authorized urls
        CorsConfiguration configuration = new CorsConfiguration()
        configuration.setAllowedOrigins(["*"])
        configuration.setAllowedMethods(["HEAD", "GET", "POST", "PUT", "DELETE", "PATCH"])// keep caps, why?
        // setAllowCredentials(true) is important, otherwise:
        // The value of the 'Access-Control-Allow-Origin' header in the response must not be the wildcard '*' when the request's credentials mode is 'include'.
        configuration.setAllowCredentials(true)
        // setAllowedHeaders is important! Without it, OPTIONS preflight request
        // will fail with 403 Invalid CORS request
        configuration.setAllowedHeaders(["Authorization", "Cache-Control", "Content-Type"])

        //todo add a cors whitelist database table vs all ?
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource()
        source.registerCorsConfiguration("/**", configuration)
        return source
    }
}
