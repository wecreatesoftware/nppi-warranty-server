package wecreate.software.nppi.warranty.application.adapters

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import wecreate.software.nppi.warranty.authentication.filters.JWTAuthenticationFilter
import wecreate.software.nppi.warranty.authentication.filters.JWTLoginFilter
import wecreate.software.nppi.warranty.authentication.handlers.BetterLogoutSuccessHandler
import wecreate.software.nppi.warranty.user.service.BetterUserDetailsService

@Configuration
class BetterWebSecurityConfigurerAdapter extends WebSecurityConfigurerAdapter {
    @Autowired
    BetterUserDetailsService betterUserDetailsService

    protected void configure(HttpSecurity http) throws Exception {
        http
            .headers().frameOptions().sameOrigin().and()
            .cors().and()
            .csrf().disable().authorizeRequests()
            .antMatchers("/admin/**").hasAuthority("admin")
            .antMatchers("/h2-console").permitAll()
            .anyRequest().authenticated()
            .and()
            .logout().logoutSuccessHandler(new BetterLogoutSuccessHandler()).and()
            .addFilterBefore(new JWTLoginFilter("/login", authenticationManager()), UsernamePasswordAuthenticationFilter)
            .addFilterBefore(new JWTAuthenticationFilter(), UsernamePasswordAuthenticationFilter)
    }

    @Autowired
    void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(betterUserDetailsService)
    }
}
