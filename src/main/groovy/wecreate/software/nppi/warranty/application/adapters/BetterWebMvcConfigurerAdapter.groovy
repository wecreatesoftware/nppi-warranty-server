package wecreate.software.nppi.warranty.application.adapters

import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.config.annotation.CorsRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter

@Configuration
class BetterWebMvcConfigurerAdapter extends WebMvcConfigurerAdapter {
    @Override
    void addCorsMappings(CorsRegistry registry) {
        //this is allowing cors for permit all requests, no authentication
        registry.addMapping("/**")

        //TODO: is this needed, every url will need at least to be authenticated ... ?
    }
}
