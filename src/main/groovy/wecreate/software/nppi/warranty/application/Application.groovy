package wecreate.software.nppi.warranty.application

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.MapperFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.databind.util.ISO8601DateFormat
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.context.annotation.Bean
import org.springframework.core.io.ClassPathResource
import org.springframework.data.jpa.repository.config.EnableJpaAuditing
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import wecreate.software.nppi.warranty.additional.entity.AdditionalInfo
import wecreate.software.nppi.warranty.address.entity.Address
import wecreate.software.nppi.warranty.announcement.entity.Announcement
import wecreate.software.nppi.warranty.announcement.repository.AnnouncementRepository
import wecreate.software.nppi.warranty.claim.entity.Claim
import wecreate.software.nppi.warranty.claim.entity.ClaimStatus
import wecreate.software.nppi.warranty.claim.repository.ClaimRepository
import wecreate.software.nppi.warranty.claim.repository.ClaimStatusRepository
import wecreate.software.nppi.warranty.customer.entity.CustomerContact
import wecreate.software.nppi.warranty.dealer.entity.DealerContact
import wecreate.software.nppi.warranty.engine.entity.EngineInfo
import wecreate.software.nppi.warranty.menu.entity.MenuItem
import wecreate.software.nppi.warranty.menu.repository.MenuItemRepository
import wecreate.software.nppi.warranty.repair.entity.RepairInfo
import wecreate.software.nppi.warranty.role.entity.Role
import wecreate.software.nppi.warranty.role.repository.RoleRepository
import wecreate.software.nppi.warranty.user.entity.User
import wecreate.software.nppi.warranty.user.repository.UserRepository

import javax.persistence.Temporal
import javax.persistence.TemporalType

@SpringBootApplication(scanBasePackages = "wecreate.software.nppi.warranty")
@EnableJpaRepositories(basePackages = "wecreate.software.nppi.warranty.*.repository")
@EntityScan(basePackages = "wecreate.software.nppi.warranty.*.entity")
@EnableJpaAuditing
@EnableWebSecurity
class Application implements CommandLineRunner {

    @Autowired
    UserRepository userRepository

    @Autowired
    RoleRepository roleRepository

    @Autowired
    AnnouncementRepository announcementRepository

    @Autowired
    MenuItemRepository menuItemRepository

    @Autowired
    ClaimRepository claimRepository

    @Autowired
    ClaimStatusRepository claimStatusRepository

    @Value("\${management.security.roles}")
    String managementSecurityRoles

    static void main(String... args) {
        SpringApplication.run(Application, args)
    }

    @Bean
    ObjectMapper objectMapper() {
        new ObjectMapper()
            .setSerializationInclusion(JsonInclude.Include.NON_NULL)
            .configure(JsonParser.Feature.ALLOW_COMMENTS, false)
            .configure(SerializationFeature.INDENT_OUTPUT, false)
            .configure(MapperFeature.DEFAULT_VIEW_INCLUSION, true)
            .setDateFormat(new ISO8601DateFormat())
    }

    @Override
    void run(String... args) throws Exception {
        roleRepository.deleteAll()
        userRepository.deleteAll()
        announcementRepository.deleteAll()
        menuItemRepository.deleteAll()
        claimStatusRepository.deleteAll()

        List<Role> adminRoles = managementSecurityRoles.split().collect {
            roleRepository.save(new Role(name: it, description: "${it} rights throughout the application"))
        }

        userRepository.save(new User(
            username: "admin",
            firstname: "admin",
            lastname: "admin",
            password: "admin",
            email: "admin@admin.com",
            roles: adminRoles
        ))

        Role userRole = roleRepository.save(new Role(name: "user", description: "Generic user of the application."))
        userRepository.save(new User(
            username: "user",
            firstname: "user",
            lastname: "user",
            password: "user",
            email: "user@user.com",
            roles: [userRole]
        ))

        20.times {
            announcementRepository.save(new Announcement(primaryText: "Announcement ${it + 1}", secondaryText: "I'll be in your neighborhood doing errands this weekend. Do you want to grab brunch?"))
        }

        List claims = new ObjectMapper().readValue(new ClassPathResource("claim.test.data.json").file, List)

        ClaimStatus claimStatusSubmitted = claimStatusRepository.save(new ClaimStatus(name: "Submitted", onCreateDefault: true))
        ClaimStatus claimStatusApproved = claimStatusRepository.save(new ClaimStatus(name: "Approved"))
        ClaimStatus claimStatusDeclined = claimStatusRepository.save(new ClaimStatus(name: "Declined"))

        List<ClaimStatus> claimStatuses = [claimStatusSubmitted, claimStatusApproved, claimStatusDeclined]

        claimRepository.save(claims.collect {
            new Claim(
                id: it.id,
                disabled: it.disabled,
                created: new Date(it.created),
                createdBy: it.createdBy,
                dealerContact: it.dealerContact,
                customerContact: it.customerContact,
                engineInfo: it.engineInfo,
                additionalInfo: it.additionalInfo,
                repairInfo: it.repairInfo,
                claimStatus: claimStatuses[new Random().nextInt(claimStatuses.size())]
            )
        })
        List<Role> allRoles = adminRoles << userRole
        menuItemRepository.save(new MenuItem(displayOrder: 0, name: "Claims", icon: "Payment", href: "/claims", roles: allRoles))
        menuItemRepository.save(new MenuItem(displayOrder: 1, name: "Training", icon: "Event", href: "/training", roles: allRoles))
        menuItemRepository.save(new MenuItem(displayOrder: 2, name: "Announcements", icon: "Announcement", href: "/announcement", roles: adminRoles))
    }
}
