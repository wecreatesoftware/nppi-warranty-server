package wecreate.software.nppi.warranty.application.entity

import com.fasterxml.jackson.annotation.JsonView
import groovy.transform.Canonical
import org.hibernate.annotations.GenericGenerator
import org.hibernate.envers.Audited
import org.springframework.data.annotation.CreatedBy
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedBy
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import wecreate.software.nppi.warranty.application.views.JsonViews

import javax.persistence.EntityListeners
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.MappedSuperclass
import javax.persistence.Temporal
import javax.persistence.TemporalType
import javax.persistence.Version

@MappedSuperclass
@Canonical
@EntityListeners(AuditingEntityListener)
abstract class DatabaseEntity implements Serializable {
    private static final long serialVersionUID = 1L

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    String id

    @Version
    Long version = 0L

    @CreatedDate
    @Temporal(TemporalType.TIMESTAMP)
    Date created

    @CreatedBy
    String createdBy

    @LastModifiedDate
    @Temporal(TemporalType.TIMESTAMP)
    Date modified

    @LastModifiedBy
    String modifiedBy

    Boolean disabled = false
}
