package wecreate.software.nppi.warranty.customer.entity

import org.hibernate.envers.Audited
import wecreate.software.nppi.warranty.address.entity.Address
import wecreate.software.nppi.warranty.application.entity.DatabaseEntity

import javax.persistence.CascadeType
import javax.persistence.Entity
import javax.persistence.OneToOne
import javax.persistence.Temporal
import javax.persistence.TemporalType

@Entity
@Audited
class CustomerContact extends DatabaseEntity {

    String customerName

    @OneToOne(cascade = CascadeType.ALL)
    Address address

    String phoneNumber

    @Temporal(TemporalType.TIMESTAMP)
    Date dateOfPurchase
}
