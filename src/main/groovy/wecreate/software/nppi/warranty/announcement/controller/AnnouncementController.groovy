package wecreate.software.nppi.warranty.announcement.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import wecreate.software.nppi.warranty.announcement.entity.Announcement
import wecreate.software.nppi.warranty.announcement.service.AnnouncementService

@RestController
@RequestMapping("announcements")
class AnnouncementController {
    @Autowired
    AnnouncementService announcementService

    @GetMapping
    List<Announcement> announcements(){
        return announcementService.findByOrderByCreatedDesc()
    }

    @PostMapping
    Announcement createAnnouncement(@RequestBody Announcement announcement){
        return announcementService.saveAnnouncement(announcement)
    }
}
