package wecreate.software.nppi.warranty.announcement.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import wecreate.software.nppi.warranty.announcement.entity.Announcement

@Repository
interface AnnouncementRepository extends JpaRepository<Announcement, String> {
    List<Announcement> findByOrderByCreatedDesc()
}