package wecreate.software.nppi.warranty.announcement.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import wecreate.software.nppi.warranty.announcement.entity.Announcement
import wecreate.software.nppi.warranty.announcement.repository.AnnouncementRepository

@Service
class AnnouncementService {
    @Autowired
    AnnouncementRepository announcementRepository

    List<Announcement> findByOrderByCreatedDesc() {
        return announcementRepository.findByOrderByCreatedDesc()
    }

   Announcement saveAnnouncement(Announcement announcement) {
        return announcementRepository.save(announcement)
    }

}
