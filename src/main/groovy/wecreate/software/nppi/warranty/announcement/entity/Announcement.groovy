package wecreate.software.nppi.warranty.announcement.entity

import org.hibernate.envers.Audited
import wecreate.software.nppi.warranty.application.entity.DatabaseEntity

import javax.persistence.Entity

@Entity
@Audited
class Announcement extends DatabaseEntity {

    String primaryText

    String secondaryText

    //String leftIcon
}
