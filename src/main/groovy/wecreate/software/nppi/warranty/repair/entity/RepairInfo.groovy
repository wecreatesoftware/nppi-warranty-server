package wecreate.software.nppi.warranty.repair.entity

import org.hibernate.envers.Audited
import wecreate.software.nppi.warranty.application.entity.DatabaseEntity

import javax.persistence.Entity

@Entity
@Audited
class RepairInfo extends DatabaseEntity {

    String repairOrderNumber

    String primaryPartFailure

    String partsCost

    String laborHours

    String laborRate

    String freightCost

    String additionalParts
}
