package wecreate.software.nppi.warranty.claim.entity

import com.fasterxml.jackson.annotation.JsonIgnore
import org.hibernate.envers.Audited
import wecreate.software.nppi.warranty.application.entity.DatabaseEntity

import javax.persistence.Entity

@Entity
@Audited
class ClaimStatus extends DatabaseEntity {

    String name

    String description

    @JsonIgnore
    Boolean onCreateDefault = false
}