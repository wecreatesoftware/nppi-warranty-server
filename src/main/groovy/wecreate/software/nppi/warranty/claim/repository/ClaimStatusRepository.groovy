package wecreate.software.nppi.warranty.claim.repository

import org.springframework.data.jpa.repository.JpaRepository
import wecreate.software.nppi.warranty.claim.entity.ClaimStatus

interface ClaimStatusRepository extends JpaRepository<ClaimStatus, String> {
    ClaimStatus findByOnCreateDefault(Boolean onCreateDefault)
}
