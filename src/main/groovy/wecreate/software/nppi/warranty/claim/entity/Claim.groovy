package wecreate.software.nppi.warranty.claim.entity

import com.fasterxml.jackson.annotation.JsonView
import org.hibernate.envers.Audited
import wecreate.software.nppi.warranty.additional.entity.AdditionalInfo
import wecreate.software.nppi.warranty.application.entity.DatabaseEntity
import wecreate.software.nppi.warranty.application.views.JsonViews
import wecreate.software.nppi.warranty.customer.entity.CustomerContact
import wecreate.software.nppi.warranty.dealer.entity.DealerContact
import wecreate.software.nppi.warranty.engine.entity.EngineInfo
import wecreate.software.nppi.warranty.repair.entity.RepairInfo

import javax.persistence.CascadeType
import javax.persistence.Entity
import javax.persistence.OneToOne

@Entity
@Audited
class Claim extends DatabaseEntity {

    @OneToOne(cascade = CascadeType.ALL)
    DealerContact dealerContact

    @OneToOne(cascade = CascadeType.ALL)
    CustomerContact customerContact

    @OneToOne(cascade = CascadeType.ALL)
    EngineInfo engineInfo

    @OneToOne(cascade = CascadeType.ALL)
    RepairInfo repairInfo

    @OneToOne(cascade = CascadeType.ALL)
    AdditionalInfo additionalInfo

    @OneToOne(cascade = CascadeType.ALL)
    ClaimStatus claimStatus
}
