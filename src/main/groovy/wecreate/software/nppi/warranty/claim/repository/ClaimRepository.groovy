package wecreate.software.nppi.warranty.claim.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository
import wecreate.software.nppi.warranty.claim.entity.Claim

@Repository
interface ClaimRepository extends JpaRepository<Claim, String>, JpaSpecificationExecutor<Claim> {
    List<Claim> findByCreatedByAndDisabledOrderByCreated(String createdBy, Boolean disabled)

    List<Claim> findByOrderByCreated()

    Claim findByCreatedByAndId(String createdBy, String id)
}
