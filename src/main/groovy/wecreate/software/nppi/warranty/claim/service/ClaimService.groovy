package wecreate.software.nppi.warranty.claim.service

import com.fasterxml.jackson.annotation.JsonView
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.domain.Specification
import org.springframework.stereotype.Service
import wecreate.software.nppi.warranty.application.views.JsonViews
import wecreate.software.nppi.warranty.claim.domain.ClaimSpecification
import wecreate.software.nppi.warranty.claim.domain.ClaimSpecificationBuilder
import wecreate.software.nppi.warranty.claim.entity.Claim
import wecreate.software.nppi.warranty.claim.entity.ClaimStatus
import wecreate.software.nppi.warranty.claim.repository.ClaimRepository
import wecreate.software.nppi.warranty.claim.repository.ClaimStatusRepository
import wecreate.software.nppi.warranty.user.service.UserService

import java.util.regex.Matcher
import java.util.regex.Pattern

@Service
class ClaimService {

    @Autowired
    ClaimRepository claimRepository

    @Autowired
    ClaimStatusRepository claimStatusRepository

    Page<Claim> search(Specification claimSpecification, Pageable pageable) {
        return claimRepository.findAll(claimSpecification, pageable)
    }

//    List<Claim> findByCreatedByOrderByCreated() {
//        return claimRepository.findByCreatedByAndDisabledOrderByCreated(UserService.loggedInUsername(), false)
//    }
//
//    List<Claim> findByOrderByCreated() {
//        return claimRepository.findByOrderByCreated()
//    }
//
    Claim create(Claim claim) {
        ClaimStatus claimStatus = claimStatusRepository.findByOnCreateDefault(true)
        claim.claimStatus = claimStatus
        return claimRepository.save(claim)
    }
//
    void delete(String id) {
        Claim claim = claimRepository.findByCreatedByAndId(UserService.loggedInUsername(), id)
        if (claim) {
            claim.disabled = true
            claimRepository.save(claim)
        }
    }
}
