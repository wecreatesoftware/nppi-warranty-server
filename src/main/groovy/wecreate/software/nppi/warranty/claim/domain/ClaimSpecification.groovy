package wecreate.software.nppi.warranty.claim.domain

import org.springframework.data.jpa.domain.Specification
import wecreate.software.nppi.warranty.application.domain.SpecificationSearchCriteria
import wecreate.software.nppi.warranty.claim.entity.Claim

import javax.persistence.criteria.CriteriaBuilder
import javax.persistence.criteria.CriteriaQuery
import javax.persistence.criteria.Predicate
import javax.persistence.criteria.Root


//TODO: break this shit out
class ClaimSpecification implements Specification<Claim> {

    SpecificationSearchCriteria criteria

    @Override
    Predicate toPredicate(Root<Claim> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
        if (criteria.operation.equalsIgnoreCase(">")) {
            if (root.get(criteria.key).javaType == Date) {
                return builder.greaterThan(root.get(criteria.key), new Date(criteria.value as Long))
            }
            return builder.greaterThan(root.<String> get(criteria.key), criteria.value.toString())
        } else if (criteria.operation.equalsIgnoreCase("<")) {
            if (root.get(criteria.key).javaType == Date) {
                return builder.lessThan(root.get(criteria.key), new Date(criteria.value as Long))
            }
            return builder.lessThan(root.<String> get(criteria.key), criteria.value.toString())
        } else if (criteria.operation.equalsIgnoreCase(">=")) {
            if (root.get(criteria.key).javaType == Date) {
                return builder.greaterThanOrEqualTo(root.get(criteria.key), new Date(criteria.value as Long))
            }
            return builder.greaterThanOrEqualTo(root.<String> get(criteria.key), criteria.value.toString())
        } else if (criteria.operation.equalsIgnoreCase("<=")) {
            if (root.get(criteria.key).javaType == Date) {
                return builder.lessThanOrEqualTo(root.get(criteria.key), new Date(criteria.value as Long))
            }
            return builder.lessThanOrEqualTo(root.<String> get(criteria.key), criteria.value.toString())
        } else if (criteria.operation.equalsIgnoreCase("contains")) {
            return builder.like(root.<String> get(criteria.key), criteria.value.toString())
        } else if (criteria.operation.equalsIgnoreCase("!contains")) {
            return builder.notLike(root.<String> get(criteria.key), criteria.value.toString())
        } else if (criteria.operation.equalsIgnoreCase(":")) {
            if (root.get(criteria.key).javaType == String) {
                return builder.like(root.<String> get(criteria.key), "%" + criteria.value + "%")
            } else {
                return builder.equal(root.get(criteria.key), criteria.value)
            }
        }
        return null
    }
}

