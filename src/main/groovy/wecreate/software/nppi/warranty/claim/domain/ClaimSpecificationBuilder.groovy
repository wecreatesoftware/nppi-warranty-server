package wecreate.software.nppi.warranty.claim.domain

import wecreate.software.nppi.warranty.application.domain.SpecificationSearchCriteria

class ClaimSpecificationBuilder {
    final List<SpecificationSearchCriteria> params

    ClaimSpecificationBuilder() {
        params = new ArrayList<SpecificationSearchCriteria>()
    }

    ClaimSpecificationBuilder with(String key, String operation, Object value) {
        params.add(new SpecificationSearchCriteria(key: key, operation: operation, value: value))
        return this
    }

    ClaimSpecification build() {
        if (params.size() == 0) {
            return null
        }

        List<ClaimSpecification> specs = new ArrayList<ClaimSpecification>()
        for (SpecificationSearchCriteria param : params) {
            specs.add(new ClaimSpecification(criteria: param))
        }

        ClaimSpecification result = specs.get(0)
        for (int i = 1; i < specs.size(); i++) {
            result = SpecificationSearchCriteria.where(result).and(specs.get(i))
        }
        return result
    }
}
