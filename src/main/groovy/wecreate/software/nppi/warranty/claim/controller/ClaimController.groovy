package wecreate.software.nppi.warranty.claim.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.web.bind.annotation.*
import wecreate.software.nppi.warranty.claim.domain.ClaimSpecification
import wecreate.software.nppi.warranty.claim.domain.ClaimSpecificationBuilder
import wecreate.software.nppi.warranty.claim.entity.Claim
import wecreate.software.nppi.warranty.claim.repository.ClaimRepository
import wecreate.software.nppi.warranty.claim.service.ClaimService
import wecreate.software.nppi.warranty.user.service.UserService

import java.util.regex.Matcher
import java.util.regex.Pattern

@RestController
@RequestMapping("claims")
class ClaimController {

    @Autowired
    ClaimService claimService

    @Autowired
    UserService userService

    @Autowired
    ClaimRepository claimRepository

    @GetMapping
    Page<Claim> search(@RequestParam(value = "search", required = false, defaultValue = "") String search,
                       @RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
                       @RequestParam(value = "size", required = false, defaultValue = "10") Integer size,
                       @RequestParam(value = "sort", required = false, defaultValue = "created") String sort,
                       @RequestParam(value = "direction", required = false, defaultValue = "desc") String direction) {
        Pageable pageable = new PageRequest(page, size, Sort.Direction.valueOf(direction.toUpperCase()), sort)
        search = userService.isAdmin() ? search : [search, "createdBy:${userService.loggedInUsername()}"].findAll().join(",")

        ClaimSpecificationBuilder builder = new ClaimSpecificationBuilder()
        Pattern pattern = Pattern.compile("(\\w+?)(:|<=|>=|<|>)(\\S+?),")
        Matcher matcher = pattern.matcher("${search},")
        while (matcher.find()) {
            builder.with(matcher.group(1), matcher.group(2), matcher.group(3))
        }

        ClaimSpecification claimSpecification = builder.build()

        return claimService.search(claimSpecification, pageable)
    }

    @PostMapping
    Claim create(@RequestBody Claim claim) {
        return claimService.create(claim)
    }

    @DeleteMapping("{id}")
    void delete(@RequestParam String id) {
        claimService.delete(id)
    }
}
