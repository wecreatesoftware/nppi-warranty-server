package wecreate.software.nppi.warranty.address.entity

import org.hibernate.envers.Audited
import wecreate.software.nppi.warranty.application.entity.DatabaseEntity

import javax.persistence.Entity

@Entity
@Audited
class Address extends DatabaseEntity {

    String street

    String city

    String state

    Integer zipCode

    Integer zipCodePlusFour

}
